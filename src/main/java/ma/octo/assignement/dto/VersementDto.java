package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class VersementDto {

    private final String fullNameEmetteur;

    private final String ribBenificiare;

    private final BigDecimal montantVersement;

    private final String motifVersement;

    private final Instant dateExecution;

    public VersementDto(String fullNameEmetteur, String ribBenificiare, BigDecimal montantVirement, String motifVersement, Instant dateExecution) {
        this.fullNameEmetteur = fullNameEmetteur;
        this.ribBenificiare = ribBenificiare;
        this.montantVersement = montantVirement;
        this.motifVersement = motifVersement;
        this.dateExecution = dateExecution;
    }

    public String getFullNameEmetteur() {
        return fullNameEmetteur;
    }

    public String getRibBenificiare() {
        return ribBenificiare;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public String getMotifVersement() {
        return motifVersement;
    }

    public Instant getDateExecution() {
        return dateExecution;
    }


}
