package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;


public interface AuditRepository extends JpaRepository<ma.octo.assignement.domain.Audit, Long> {
}
