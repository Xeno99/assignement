package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//TODO we should add @Respository annotation to all repositories
public interface CompteRepository extends JpaRepository<Compte, Long> {
  //TODO we should use Optional instead of the Compte class
   Optional<Compte>    findByNumeroCompte(String numeroCompte);
   Optional<Compte>     findByRib(String rib);
}
