package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class VersementService {

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AuditService auditService;


    public List<VersementDto> getAllVersement(){return
            versementRepository
                    .findAll()
                     .stream()
                     .map(VersementMapper::map)
                     .collect(Collectors.toList());
    }

    public void createVersement(VersementDto versementDto) throws CompteNonExistantException {

        Optional<Compte> optionalBenificiare = compteService.findByRib(versementDto.getRibBenificiare());

        if (optionalBenificiare.isEmpty()) {
            LOGGER.error("Compte Recepteur Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        Compte benificiare=optionalBenificiare.get();
        BigDecimal montantVersement=versementDto.getMontantVersement();
        BigDecimal benificiareSolde=benificiare.getSolde();

        //Add the money the benificiare account
        benificiare.setSolde(benificiareSolde.add(montantVersement));
        compteService.save(benificiare);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(benificiare);
        versement.setFullNameEmetteur(versementDto.getFullNameEmetteur());
        versement.setMontantVersement(montantVersement);

        versementRepository.save(versement);

        auditService.auditOperation("Versement depuis " + versementDto.getFullNameEmetteur() + " vers " + versementDto.getRibBenificiare() + " d'un montant de " + montantVersement
                .toString(), EventType.VERSEMENT);
    }
}
