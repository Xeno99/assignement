package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompteService {

    @Autowired
    private CompteRepository compteRepository;

    public List<Compte> getAllComptes(){
       return compteRepository.findAll();
    }

    public  Optional<Compte> findByNumeroCompte(String numeroCompte){

        return compteRepository.findByNumeroCompte(numeroCompte);
    }
    public  Optional<Compte> findByRib(String rib){

        return compteRepository.findByRib(rib);
    }
    public void save(Compte compte) {
        compteRepository.save(compte);
    }

}
