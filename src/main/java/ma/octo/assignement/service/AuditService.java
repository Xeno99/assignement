package ma.octo.assignement.service;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

///TODO we should merge the 2 functions to one
@Service
@Transactional
public class AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditRepository auditRepository;

    public void auditOperation(String message,EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        ma.octo.assignement.domain.Audit audit = new ma.octo.assignement.domain.Audit();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditRepository.save(audit);
    }


    //TODO this function is useless now
    /*public void auditVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        ma.octo.assignement.domain.Audit audit = new ma.octo.assignement.domain.Audit();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }*/
}
