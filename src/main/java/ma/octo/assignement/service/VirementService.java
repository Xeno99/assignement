package ma.octo.assignement.service;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class VirementService {

    //public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000L);

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AuditService auditService;


    public List<VirementDto> getAllVirements(){
        return virementRepository.
                findAll().
                stream()
                .map(VirementMapper::map)
                .collect(Collectors.toList());
        //return  ;
    }

    public void createVirement( VirementDto virementDto)
             throws CompteNonExistantException,SoldeDisponibleInsuffisantException {
       Optional<Compte>   optionalEmetteur = compteService.findByNumeroCompte(virementDto.getNumeroCompteEmetteur());
       Optional<Compte>    optionalBenificiare = compteService.findByNumeroCompte(virementDto.getNumeroCompteBeneficiaire());


        if (optionalEmetteur.isEmpty()) {
            LOGGER.error("Compte Emetteur Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (optionalBenificiare.isEmpty()) {
            LOGGER.error("Compte Recepteur Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        Compte emeteur=optionalEmetteur.get();
        Compte benificiare=optionalBenificiare.get();


        //TODO we should throw an exception in this line and delete duplicate verification
        BigDecimal montantVirement=virementDto.getMontantVirement();
        BigDecimal emeteurSolde=emeteur.getSolde();
        BigDecimal benificiareSolde=benificiare.getSolde();
        if (emeteurSolde.compareTo(montantVirement)==-1 ) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw  new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        emeteur.setSolde(emeteurSolde.subtract(montantVirement));
        compteService.save(emeteur);
        benificiare.setSolde(benificiareSolde.add(montantVirement));
        compteService.save(benificiare);

        //TODO we shouldn't  use intValue() function because we loose money in the transaction process



        //TODO they forgot to add the motif attribute but tey should
        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(benificiare);
        virement.setCompteEmetteur(emeteur);
        virement.setMontantVirement(montantVirement);

        virementRepository.save(virement);

        auditService.auditOperation("Virement depuis " + virementDto.getNumeroCompteEmetteur() + " vers " + virementDto
                .getNumeroCompteBeneficiaire() + " d'un montant de " + montantVirement
                .toString(), EventType.VIREMENT);
    }



}
