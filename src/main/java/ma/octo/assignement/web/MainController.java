package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
//TODO we should replace sysout with logger.info/warn/error
@RestController(value = "/virements")
@Validated
class MainController {


    Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private CompteService compteService;
    @Autowired
    private VirementService virementService;
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private VersementService versementService;

    @GetMapping("lister_virements")
    List<VirementDto> loadAll() {
        return virementService.getAllVirements();
    }

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
       return  compteService.getAllComptes();
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.getAllUtilisateurs();
    }

    @GetMapping("lister_versements")
    List<VersementDto> loadAllVersements() { return versementService.getAllVersement(); }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(
            @Valid @NotEmpty(message = "Numero de compte Emetteur  null") @RequestParam String numeroCompteEmetteur,
            @Valid @NotEmpty(message = "Numero de Compte Beneficiare  null") @RequestParam String numeroCompteBeneficiaire,
            @Valid @NotEmpty(message = "Motif null") @RequestParam String motif,
            @Valid @Min(value = 10,message = "Value Should be > 10") @Max(value = 10000,message = "Value should be < 10000")  @RequestParam BigDecimal montantVirement,
            @RequestParam Instant date )
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
             VirementDto virementDto=new VirementDto(numeroCompteEmetteur,numeroCompteBeneficiaire,motif,montantVirement,date);
          virementService.createVirement(virementDto);
    }

    @PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVersement(
            @Valid @NotEmpty(message = "Nom Emetteur  Empty") @RequestParam String fullNameEmetteur,
            @Valid @NotEmpty(message = "Rib de Compte Beneficiare  Empty") @RequestParam String ribBenificiare,
            @Valid @NotEmpty(message = "Motif Empty") @RequestParam String motifVersement,
            @Valid @Min(value = 10,message = "Value Should be > 10") @Max(value = 10000,message = "Value should be < 10000")  @RequestParam BigDecimal montantVersement,
            @RequestParam Instant dateExecution )
            throws  CompteNonExistantException {
        VersementDto versementDto=new VersementDto(fullNameEmetteur,ribBenificiare,montantVersement,motifVersement,dateExecution);

        versementService.createVersement(versementDto);
    }

}
