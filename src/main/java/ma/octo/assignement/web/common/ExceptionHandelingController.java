package ma.octo.assignement.web.common;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;

@ControllerAdvice
public class ExceptionHandelingController {

    Logger LOGGER = LoggerFactory.getLogger(ExceptionHandelingController.class);
    @ExceptionHandler(SoldeDisponibleInsuffisantException.class)
    public ResponseEntity<String> handleSoldeDisponibleInsuffisantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Pas de solde pas de virement", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
    }

    @ExceptionHandler(CompteNonExistantException.class)
    public ResponseEntity<String> handleCompteNonExistantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler( value = {DateTimeParseException.class})
    public ResponseEntity<String> handleDateFormatException(
            DateTimeParseException exception) {
        //exception.printStackTrace();
        LOGGER.error(exception.getMessage());
        return  new ResponseEntity<>("Date Format Invalid",null,400);
    }
    @ExceptionHandler( value = {ConstraintViolationException.class})
    public ResponseEntity<String> handleConstraintViolationException(
            ConstraintViolationException exception) {
        exception.printStackTrace();
        LOGGER.error(exception.getMessage());
        return  new ResponseEntity<>(exception.getMessage(),null,400);
    }
}
