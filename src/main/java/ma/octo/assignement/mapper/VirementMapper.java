package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {



    //TODO this method should fill all VirmentDTO fields
    public static VirementDto map(Virement virement) {

       return new VirementDto(
                virement.getCompteEmetteur().getNumeroCompte(),
                virement.getCompteBeneficiaire().getNumeroCompte(),
                virement.getMotifVirement(),
                virement.getMontantVirement() ,
                virement.getDateExecution() );


    }

}
