package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {


    //TODO this method should fill all VirmentDTO fields
    public static VersementDto map(Versement versement) {

        return new VersementDto(
                versement.getFullNameEmetteur(),
                versement.getCompteBeneficiaire().getNumeroCompte(),
                versement.getMontantVersement() ,
                versement.getMotifVersement(),
                versement.getDateExecution() );


    }
}
