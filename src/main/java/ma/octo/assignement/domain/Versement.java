package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "VERSEMENT")
public class Versement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 5, scale = 2)
  private BigDecimal montantVersement;

  @Column
  //TODO  deprecated Type we should use TimeStamp/Instant
  private Instant dateExecution;

  @Column
  private String fullNameEmetteur;

  //TODO l'argent doit etre verser vers un seul compte recepteur
  @OneToOne
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifVersement;

  public BigDecimal getMontantVersement() {
    return montantVersement;
  }

  public void setMontantVersement(BigDecimal montantVersement) {
    this.montantVersement = montantVersement;
  }

  public void setDateExecution(Instant dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Instant getDateExecution() {
    return dateExecution;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifVersement() {
    return motifVersement;
  }

  public void setMotifVersement(String motifVirement) {
    this.motifVersement = motifVirement;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFullNameEmetteur() {
    return fullNameEmetteur;
  }

  public void setFullNameEmetteur(String fullNameEmetteur) {
    this.fullNameEmetteur = fullNameEmetteur;
  }
}
