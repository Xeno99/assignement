package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "VIREMENT")
public class Virement {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 5, scale = 2)
  private BigDecimal montantVirement;

  @Column
  //TODO  deprecated Type we should use TimeStamp
  private Instant dateExecution;

  //TODO le virement doit etre fait un seul compte emeteur (source)
  @OneToOne
  private Compte compteEmetteur;

  //TODO l'argent doit etre verser vers un seul compte recepteur
  @OneToOne
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifVirement;

  public Virement(BigDecimal montantVirement, Instant dateExecution, Compte compteEmetteur, Compte compteBeneficiaire, String motifVirement) {
    this.montantVirement = montantVirement;
    this.dateExecution = dateExecution;
    this.compteEmetteur = compteEmetteur;
    this.compteBeneficiaire = compteBeneficiaire;
    this.motifVirement = motifVirement;
  }
  public Virement(){}

  public BigDecimal getMontantVirement() {
    return montantVirement;
  }

  public void setMontantVirement(BigDecimal montantVirement) {
    this.montantVirement = montantVirement;
  }

  public Instant getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Instant dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Compte getCompteEmetteur() {
    return compteEmetteur;
  }

  public void setCompteEmetteur(Compte compteEmetteur) {
    this.compteEmetteur = compteEmetteur;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifVirement() {
    return motifVirement;
  }

  public void setMotifVirement(String motifVirement) {
    this.motifVirement = motifVirement;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
