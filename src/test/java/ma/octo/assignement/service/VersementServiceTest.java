package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VersementServiceTest {

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private  VersementService versementService;

    Compte compte1 = new Compte();


    @BeforeEach
    void setUp() {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user47");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");
        utilisateur1.setBirthdate(new Date());


        utilisateurRepository.save(utilisateur1);


        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteRepository.save(compte1);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createVersement() throws CompteNonExistantException {
        BigDecimal solde=compte1.getSolde();
        versementService.createVersement(new VersementDto("Mamamia","RIB1",BigDecimal.valueOf(100),"Rnd", Instant.now()));
        BigDecimal soldeAfter=new BigDecimal(100);
        soldeAfter= soldeAfter.setScale(2);
        Assertions
                .assertEquals(
                        solde.add( soldeAfter),
                        compteRepository.findByNumeroCompte("010000A000001000"
                        )
                                .get()
                                .getSolde()
                );
    }
    @Test
    void getAllVersement() throws CompteNonExistantException {
        BigDecimal solde=new BigDecimal(100);
        solde= solde.setScale(2);
        VersementDto versementDtoExpected=new VersementDto("Mamamia","RIB1",solde,"Rnd", Instant.now());
        versementService.createVersement(versementDtoExpected);
        VersementDto versementDtoActual=versementService.getAllVersement().get(0);
        Assertions.assertEquals( versementDtoExpected.getMontantVersement(),versementDtoActual.getMontantVersement());
        Assertions.assertEquals( versementDtoExpected.getFullNameEmetteur(),versementDtoActual.getFullNameEmetteur());
        //Assertions.assertEquals( versementDtoExpected.getDateExecution(),versementDtoActual.getDateExecution());
    }
}